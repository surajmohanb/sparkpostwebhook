var express = require('express')
var bodyParser = require('body-parser')

var app = express()
const PORT = process.env.PORT || 5000;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
/*
app.use(function (req, res) {
  res.setHeader('Content-Type', 'text/plain')
  res.write('you posted:\n')
  res.end(JSON.stringify(req.body, null, 2))
});*/

app.get("/", function(req, res){
  res.json({
    text: "Hello World"
  })
})

app.get("/webhook-target", function(req, res){
  console.log("Webhook ------------------ GET");
  console.log(req.params);
  res.json({
    text: "GET"
  })
})
app.post("/webhook-target", function(req, res){
  console.log("Webhook ------------------ POST");
  console.log(req.body);
  res.json({
    text: "POST"
  });
})

app.listen(PORT, function(){
    console.log("Server started at port "+ PORT)
})